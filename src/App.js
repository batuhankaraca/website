import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import './Assets/css/default.css';
import Header from './components/headerComponent/header';
import Footer from './components/footerComponent/footer';
import HomePage from './components/pages/homepage';
import Contact from './components/pages/contact';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <Header/>

        <Route exact path ='/' component={HomePage} />
        <Route exact path ='/Contact' component={Contact} />

        <Footer/>
      </div>
      </Router>
    );
  }
}

export default App;
